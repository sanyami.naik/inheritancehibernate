package dao;

import com.Book;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.*;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static dao.ConfigrationClass.configuration;
import static dao.ConfigrationClass.sessionFactory;

public class BookDaoClass implements BookDao{

    public void insertRecord(Book book)
    {
        Session session= sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        int i= (int) session.save(book);
        if(i>=0)
        {

            transaction.commit();
            System.out.println("The book was inserted succesfully");
        }
        else{
            System.out.println("Book not inserted");
        }
        session.close();
    }


    public List<Book> retrieveRecord() {
        Session session = sessionFactory.openSession();

        Query query = session.createQuery("from Book");
        List<Book> list = (List<Book>) ((org.hibernate.query.Query<?>) query).list();
        return list;

    }

    public void updateRecord(int bookId,int price)
    {
        Session session= sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        Query query= session.createQuery("update Book set bookPrice=:x where bookId=:y");
        query.setParameter("x",price);
        query.setParameter("y",bookId);

        int i=query.executeUpdate();
        if(i>0)
        {
            System.out.println("The book price was updated succesfully");
        }
        else{
            System.out.println("The book price was not updated succesfully");
        }

    }


    public void deleteRecord(int bookId) {
        Session session= sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        Query query= session.createQuery("Delete from Book where bookId=:y");
        query.setParameter("y",bookId);

        int i=query.executeUpdate();
        if(i>0)
        {
            transaction.commit();
            System.out.println("The book price was updated succesfully");
        }
        else{
            System.out.println("The book price was not updated succesfully");
        }
    }

    //fetch by restrictions
    public List<Book> fetchByPrice(int greaterPrice)
    {
        Session session=configuration.buildSessionFactory().openSession();
        Criteria criteria=session.createCriteria(Book.class);  //select * from
        criteria.add(Restrictions.gt("bookPrice",greaterPrice)); // where price>greaterthan given by user
        return criteria.list();
    }

    //
    public List<Book> fetchByOrder()
    {
        Session session=configuration.buildSessionFactory().openSession();
        Criteria criteria=session.createCriteria(Book.class);  //select * from
        criteria.addOrder(Order.desc("bookPrice"));
        return criteria.list();
    }

    public void projectionUsed()
    {
        Session session= sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Book.class);
        Projection projection1=Projections.property("bookName");
        Projection projection2=Projections.property("bookPrice");

        ProjectionList projectionList=Projections.projectionList();
        projectionList.add(projection1);
        projectionList.add(projection2);

        criteria.setProjection(projectionList);

        List<Book> l=criteria.list();
        Iterator it=l.iterator();

        while(it.hasNext())
        {
            Object ob[] = (Object[])it.next();
            System.out.println(ob[0]+"--------"+ob[1]);
        }

    }

    public void combinedQuery()
    {
        Session session= sessionFactory.openSession();
        Criteria criteria=session.createCriteria(Book.class);
        Projection projection1=Projections.property("bookName");
        Projection projection2=Projections.property("bookPrice");

        ProjectionList projectionList=Projections.projectionList();
        projectionList.add(projection1);
        projectionList.add(projection2);

        criteria.setProjection(projectionList);
        criteria.add(Restrictions.gt("bookPrice",400)).addOrder(Order.asc("bookPrice"));

        List<Book> l=criteria.list();
        Iterator it=l.iterator();

        while(it.hasNext())
        {
            Object ob[] = (Object[])it.next();
            System.out.println(ob[0]+"--------"+ob[1]);

        }




    }
}
