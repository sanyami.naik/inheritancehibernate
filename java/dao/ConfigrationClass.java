package dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConfigrationClass {
    static Configuration configuration=new Configuration().configure();
    static SessionFactory sessionFactory= configuration.buildSessionFactory();
}
