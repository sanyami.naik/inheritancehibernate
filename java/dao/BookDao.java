package dao;

import com.Book;

import java.util.List;

public interface BookDao {

    void insertRecord(Book book);
    List<Book> retrieveRecord();

    void updateRecord(int bookId,int bookPrice);

    void deleteRecord(int bookId);



}
