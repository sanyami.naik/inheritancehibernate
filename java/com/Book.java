package com;


import javax.persistence.*;

@Entity

public class Book {
@Id
        @GeneratedValue(strategy = GenerationType.AUTO)
    int bookId;
    String bookName;
    String authorName;
    int bookPrice;

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorname) {
        this.authorName = authorname;
    }

    public int getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(int bookPrice) {
        this.bookPrice = bookPrice;
    }


    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", authorname='" + authorName + '\'' +
                ", bookPrice='" + bookPrice + '\'' +
                '}';
    }
}
