package com;

import dao.BookDaoClass;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class MainApplication {
    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        while (true) {

            System.out.println("Press 1 for adding book");
            System.out.println("Press 2 for displaying the available books");
            System.out.println("Press 3 for updating book price");
            System.out.println("Press 4 for deleting a book");
            System.out.println("Press 5 for having the price criteria");
            System.out.println("Press 6 for having the price DESC");
            System.out.println("Press 7 for having the projection");
            System.out.println("Press 8 for having the combined query");
            System.out.println("Enter the option you want");
            int option = Integer.parseInt(bufferedReader.readLine());

            switch (option) {
                case 1:
                    System.out.println("Enter book details =>bookName,authorname and price");
                    String bookName= bufferedReader.readLine();
                    String authorname=bufferedReader.readLine();
                    int bookPrice=Integer.parseInt(bufferedReader.readLine());
                    Book book=new Book();
                    book.setBookName(bookName);
                    book.setAuthorName(authorname);
                    book.setBookPrice(bookPrice);

                    new BookDaoClass().insertRecord(book);
                    break;


                case 2:
                    System.out.println("The list of books is");
                    List<Book> bookList=new BookDaoClass().retrieveRecord();
                    bookList.stream().forEach(System.out::println);
                    break;


                case 3:
                    System.out.println("Enter the id of book you want to update price of");
                    int bookId=Integer.parseInt(bufferedReader.readLine());
                    System.out.println("Enter the new price of the book");
                    int bookNewPrice=Integer.parseInt(bufferedReader.readLine());
                    new BookDaoClass().updateRecord(bookId,bookNewPrice);
                    break;


                case 4:
                    System.out.println("Enter the id of book you want to delete");
                    int bookIdToDelete=Integer.parseInt(bufferedReader.readLine());
                    new BookDaoClass().deleteRecord(bookIdToDelete);
                    break;

                case 5:
                    System.out.println("Enter the price of book for having greater ");
                    int greaterPrice=Integer.parseInt(bufferedReader.readLine());
                    System.out.println(new BookDaoClass().fetchByPrice(greaterPrice));
                    break;

                case 6:
                    System.out.println("The list by ordering by descending price is");
                    List<Book> list=new BookDaoClass().fetchByOrder();
                    list.stream().forEach(System.out::println);
                     break;


                case 7:
                    new BookDaoClass().projectionUsed();
                    break;



                case 8:
                    new BookDaoClass().combinedQuery();
                    break;

            }
        }
    }
}
